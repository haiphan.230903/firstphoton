# FirstPhoton



## Getting started

Đây là một con game mình làm ra trong quá trình lần đầu tìm hiểu Photon Networking trong Unity để làm game online.
Game gồm có 2 người chơi, chơi theo dạng lưới và theo lượt. Mỗi lượt một người chơi chỉ có thể đi 1 hoặc 2 bước theo đường thẳng. Các ô đứng sẽ bị biến mất dần mỗi khi người chơi đi ra khỏi nó. Mục đích của mỗi người chơi là cố gắng trụ lại trên bàn chơi, ai trụ lại được lâu hơn thì người đó sẽ là người chiến thắng.

## Update
Update: Thêm chế độ chơi với máy với 3 cấp độ.

## Link
- [ ] [Video demo](https://youtu.be/5JPeTArYkf8)
- [ ] [Game build](https://drive.google.com/file/d/1ItzzILnfLYrV4kUYagpIJRr6HMq7dcCt/view?usp=sharing)

