using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using NaughtyAttributes;

public class Player : MonoBehaviour
{
    public PhotonView photonView;
    public int playerIndexi, playerIndexj;
    bool isMoving, isHoldMoveFar, isWaitAnim;
    public int turnID;
    int oldi, oldj;
    [SerializeField] ParticleSystem vfxDush;
    [SerializeField] SpriteRenderer sprRen;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip moveClip, fallClip, prepareMoveFarClip;
    [SerializeField] MinimaxAI minimaxAI;

    [SerializeField,ReadOnly] int numberMoveValid;

    public bool isBot;
    bool isBotCal = false;

    private void Update()
    {
        if (photonView != null && photonView.isMine)
        {
            CheckInput();
        }
        else
        {
            if (isBot == false && turnID == GameplayController.instance.currentTurn)
            {
                CheckInput();
            }
            else if (isBotCal == false && turnID == GameplayController.instance.currentTurn)
            {
                isBotCal = true;
                BotMove();
            }
        }
    }
    void CheckInput()
    {
        if (GameplayController.instance.gameStatus != Enums.GameStatus.Playing || GameplayController.instance.currentTurn != turnID)
        {
            return;
        }

        float xAxis = Input.GetAxisRaw("Horizontal");
        float yAxis = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(KeyCode.M))
        {
            isHoldMoveFar = true;
            audioSource.clip = prepareMoveFarClip;
            audioSource.Play();
            CheckPosCanMove();
        }
        if (Input.GetKeyUp(KeyCode.M))
        {
            isHoldMoveFar = false;
            audioSource.clip = prepareMoveFarClip;
            audioSource.Play();
            CheckPosCanMove();
        }

        Move(xAxis, yAxis);
    }
    public void SetUp(int i, int j, Vector2 pos, Color color)
    {
        playerIndexi = i;
        playerIndexj = j;
        transform.position = pos;
        CheckPosCanMove();
        float r = color.r;
        float g = color.g;
        float b = color.b;
        if (photonView != null)
        {
            photonView.RPC("SyncColor", PhotonTargets.All, r, b, g);
        }
        else
        {
            SyncColor(r, b, g);
        }
    }
    [PunRPC]
    void SyncColor(float r, float b, float g)
    {
        Color c = new Color(r, b, g, 1);
        sprRen.color = c;
        vfxDush.startColor = c;
    }
    void Move(float xAxis, float yAxis)
    {
        if (isMoving || isWaitAnim) return;
        if (xAxis != 0 || yAxis != 0)
        {
            oldi = playerIndexi;
            oldj = playerIndexj;
        }

        if (xAxis > 0)
        {
            if (isHoldMoveFar == false)
            {
                playerIndexj++;
                if (playerIndexj < GameConstant.MAX_WIDTH)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                transform.DOMoveX(transform.position.x + 1.2f, 0.5f).OnComplete(() => EndMove());
            }
            else
            {
                playerIndexj += 2;
                if (playerIndexj < GameConstant.MAX_WIDTH)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                GridController.Instance.PassThrough(playerIndexi, playerIndexj - 1, 0.2f);
                transform.DOMoveX(transform.position.x + 2.4f, 0.5f).OnComplete(() => EndMove());
            }
        }
        else if (xAxis < 0)
        {

            if (isHoldMoveFar == false)
            {
                playerIndexj--;
                if (playerIndexj >= 0)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                transform.DOMoveX(transform.position.x - 1.2f, 0.5f).OnComplete(() => EndMove());
            }
            else
            {
                playerIndexj -= 2;
                if (playerIndexj >= 0)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                GridController.Instance.PassThrough(playerIndexi, playerIndexj + 1, 0.2f);
                transform.DOMoveX(transform.position.x - 2.4f, 0.5f).OnComplete(() => EndMove());
            }
        }
        else if (yAxis > 0)
        {

            if (isHoldMoveFar == false)
            {
                playerIndexi--;
                if (playerIndexi >= 0)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                transform.DOMoveY(transform.position.y + 1.2f, 0.5f).OnComplete(() => EndMove());
            }
            else
            {
                playerIndexi -= 2;
                if (playerIndexi >= 0)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                GridController.Instance.PassThrough(playerIndexi + 1, playerIndexj, 0.2f);
                transform.DOMoveY(transform.position.y + 2.4f, 0.5f).OnComplete(() => EndMove());
            }

        }
        else if (yAxis < 0)
        {

            if (isHoldMoveFar == false)
            {
                playerIndexi++;
                if (playerIndexi < GameConstant.MAX_HEIGHT)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                transform.DOMoveY(transform.position.y - 1.2f, 0.5f).OnComplete(() => EndMove());
            }
            else
            {
                playerIndexi += 2;
                if (playerIndexi < GameConstant.MAX_HEIGHT)
                {
                    if (GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] > 1)
                    {
                        InvalidMove();
                        return;
                    }
                }
                GridController.Instance.PassThrough(playerIndexi - 1, playerIndexj, 0.2f);
                transform.DOMoveY(transform.position.y - 2.4f, 0.5f).OnComplete(() => EndMove());
            }
        }

        if (xAxis != 0 || yAxis != 0)
        {
            audioSource.clip = moveClip;
            audioSource.Play();
            isMoving = true;
            GridController.Instance.PassThrough(oldi, oldj, 0, true);
            GridController.Instance.EndCheckPosCanMove(oldi, oldj);
            GridController.Instance.SetPlayerOnGrid(playerIndexi, playerIndexj, isBot);
        }
    }
    void BotMove()
    {
        Debug.Log("Bot move!");
        if (GameplayController.instance.gameStatus != Enums.GameStatus.Playing || GameplayController.instance.currentTurn != turnID)
        {
            return;
        }

        int xAsis = 1, yAsis = 0;

        if (GridController.Instance.NumberMoveValid(playerIndexi, playerIndexj) > 0)
        {
            minimaxAI.GetMove(2);
            xAsis = minimaxAI.xAxis;
            yAsis = minimaxAI.yAxis;
            isHoldMoveFar = minimaxAI.isMoveFar;
        }
        else
        {
            Debug.Log("Bot ko con duong di");
        }
        //Debug.Log(xAsis + " " + yAsis + " " + (playerIndexj + xAsis * (isMoveFar + 1)) + " " + (playerIndexi + yAsis * (isMoveFar + 1)) + " " + GridController.Instance.IsPosInvalid(playerIndexj + xAsis * (isMoveFar + 1), playerIndexi + yAsis * (isMoveFar + 1)));
        Move(xAsis, yAsis);
    }
    void RandomMove()
    {
        Debug.Log("Bot random move!");
        if (GameplayController.instance.gameStatus != Enums.GameStatus.Playing || GameplayController.instance.currentTurn != turnID)
        {
            return;
        }

        int xAsis = 1, yAsis = 0, isMoveHorizontal = 0, isMoveFar = 0;

        if (GridController.Instance.NumberMoveValid(playerIndexi, playerIndexj) > 0)
        {
            do
            {
                xAsis = Random.Range(-100, 100);
                yAsis = Random.Range(-100, 100);
                isMoveHorizontal = Random.Range(0, 100) % 2;
                isMoveFar = Random.Range(0, 100) % 2;

                if (isMoveFar == 1) isHoldMoveFar = true;
                else isHoldMoveFar = false;
                if (isMoveHorizontal == 1)
                {
                    yAsis = 0;
                    if (xAsis < 0) xAsis = -1;
                    else xAsis = 1;
                }
                else
                {
                    xAsis = 0;
                    if (yAsis < 0) yAsis = -1;
                    else yAsis = 1;
                }
            }
            while (GridController.Instance.IsPosInvalid(playerIndexi - yAsis * (isMoveFar + 1), playerIndexj + xAsis * (isMoveFar + 1)) == true);
        }
        else
        {
            Debug.Log("Bot ko con duong di");
        }
        //Debug.Log(xAsis + " " + yAsis + " " + (playerIndexj + xAsis * (isMoveFar + 1)) + " " + (playerIndexi + yAsis * (isMoveFar + 1)) + " " + GridController.Instance.IsPosInvalid(playerIndexj + xAsis * (isMoveFar + 1), playerIndexi + yAsis * (isMoveFar + 1)));
        Move(xAsis, yAsis);
    }
    void EndMove()
    {
        isMoving = false;
        CheckDie();
        //if (photonView.isMine == false)
        //{
            CheckPosCanMove();
        //}
        if (isBot)
        {
            isBotCal = false;
        }

        numberMoveValid = GridController.Instance.NumberMoveValid(playerIndexi, playerIndexj);
    }
    void CheckDie()
    {
        if (playerIndexi < 0 || playerIndexi >= GameConstant.MAX_HEIGHT || 
            playerIndexj < 0 || playerIndexj >= GameConstant.MAX_WIDTH ||
            GridController.Instance.dataGrid[playerIndexi].arr[playerIndexj] == 0) Die();
        else
        {
            GameplayController.instance.NextTurn();
        }
    }
    void Die()
    {
        Debug.Log("Die");
        audioSource.clip = fallClip;
        audioSource.Play();
        vfxDush.loop = false;
        transform.DOScale(1.3f, 0.3f).OnComplete(
            () => transform.DOScale(0, 0.7f));
        GameplayController.instance.EndGame(turnID);
    }
    void CheckPosCanMove()
    {
        if (isMoving /*|| GameplayController.instance.gameStatus != Enums.GameStatus.Playing || GameplayController.instance.currentTurn != turnID*/) return;
        GridController.Instance.CheckPosCanMove(playerIndexi, playerIndexj, isHoldMoveFar);
    }
    [Button]
    void InvalidMove()
    {
        playerIndexi = oldi;
        playerIndexj = oldj;
        isWaitAnim = true;
        Vector2 oldPos = transform.position;
        transform.DOShakePosition(0.25f,0.8f,15).OnComplete(
            () => transform.DOMove(oldPos, 0.1f).OnComplete(
                () => isWaitAnim = false));
    }
}
