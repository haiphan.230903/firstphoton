using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GridController;

public class MinimaxAI : MonoBehaviour
{
    [ReadOnly]
    public int bestMoveX, bestMoveY, xAxis, yAxis;
    int moveX, moveY, defautDepth;
    [ReadOnly]
    public bool isMoveFar;

    [SerializeField,ReadOnly] int minimaxValue;
    bool GameOver(DataGrid[] grid)
    {
        int playerRemain = 0;
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j<GameConstant.MAX_WIDTH; j++)
            {
                if (grid[i].arr[j] == 2 || grid[i].arr[j] == 3) playerRemain++;
            }    
        }
        if (playerRemain > 1) return false;
        return true;
    }

    int Evaluate(DataGrid[] grid)
    {
        int evaluate = 0;
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                if (grid[i].arr[j] == 3)
                {
                    evaluate += 15;
                    evaluate += GridController.Instance.NumberMoveValid(i, j) * 3;
                }
                if (grid[i].arr[j] == 2)
                {
                    evaluate -= 15;
                    evaluate -= GridController.Instance.NumberMoveValid(i, j);
                }
            }
        }
        return evaluate;
    }
    public int MiniMax(DataGrid[] curGrid, int depth, bool maximizingPlayer)
    {
        if (depth == 0 || GameOver(curGrid))
        {
            return Evaluate(curGrid);
        }

        if (maximizingPlayer)
        {
            int bestValue = int.MinValue;
            for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
            {
                for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
                {
                    if (curGrid[i].arr[j] == 3)
                    {
                        for (int x = -2; x <= 2; x++)
                        {
                            if ((j + x < 0) || (j + x >= GameConstant.MAX_WIDTH) || x == 0 || curGrid[i].arr[j+x]!=1)
                                continue;
                            if (defautDepth == depth)
                            {
                                moveX = x;
                                moveY = 0;
                            }

                            DataGrid[] newGrid = GridController.Instance.SimulateGrid(curGrid, x, 0, i, j, true);
                            int value = MiniMax(newGrid, depth - 1, false);
                            if (value > bestValue)
                            {
                                bestValue = value;
                                if (depth == defautDepth)
                                {
                                    bestMoveX = moveX;
                                    bestMoveY = moveY;
                                }
                            }
                            //bestValue = Mathf.Max(bestValue, value);
                        }
                        for (int y = -2; y <= 2; y++)
                        {
                            if ((i - y < 0) || (i - y >= GameConstant.MAX_HEIGHT) || y == 0 || curGrid[i-y].arr[j] != 1)
                                continue;
                            if (defautDepth == depth)
                            {
                                moveX = 0;
                                moveY = y;
                            }

                            DataGrid[] newGrid = GridController.Instance.SimulateGrid(curGrid, 0, y, i, j, true);
                            int value = MiniMax(newGrid, depth - 1, false);
                            if (value > bestValue)
                            {
                                bestValue = value;
                                if (depth == defautDepth)
                                {
                                    bestMoveX = moveX;
                                    bestMoveY = moveY;
                                }
                            }
                            //bestValue = Mathf.Max(bestValue, value);
                        }
                    }
                }
            }
            return bestValue;
        }
        else
        {
            int bestValue = int.MaxValue;
            for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
            {
                for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
                {
                    if (curGrid[i].arr[j] == 2)
                    {
                        for (int x = -2; x <= 2; x++)
                        {
                            if ((j + x < 0) || (j + x >= GameConstant.MAX_WIDTH) || x == 0 || curGrid[i].arr[j + x] != 1)
                                continue;
                            DataGrid[] newGrid = GridController.Instance.SimulateGrid(curGrid, x, 0, i, j, false);
                            int value = MiniMax(newGrid, depth - 1, true);
                            if (value < bestValue)
                            {
                                bestValue = value;
                                if (depth == defautDepth)
                                {
                                    bestMoveX = moveX;
                                    bestMoveY = moveY;
                                }
                            }
                            //bestValue = Mathf.Min(bestValue, value);
                        }
                        for (int y = -2; y <= 2; y++)
                        {
                            if ((i - y < 0) || (i - y >= GameConstant.MAX_HEIGHT) || y == 0 || curGrid[i-y].arr[j] != 1)
                                continue;
                            DataGrid[] newGrid = GridController.Instance.SimulateGrid(curGrid, 0, y, i, j, false);
                            int value = MiniMax(newGrid, depth - 1, true);
                            if (value < bestValue)
                            {
                                bestValue = value;
                                if (depth == defautDepth)
                                {
                                    bestMoveX = moveX;
                                    bestMoveY = moveY;
                                }
                            }
                            //bestValue = Mathf.Min(bestValue, value);
                        }
                    }
                }
            }
            return bestValue;
        }
    }

    public void GetMove(int depth)
    {
        defautDepth = depth;
        minimaxValue = MiniMax(GridController.Instance.dataGrid, depth, true);
        isMoveFar = false;
        if (bestMoveX < 2 && bestMoveX > -2)
        {
            xAxis = bestMoveX;
        }
        else
        {
            xAxis = bestMoveX / 2;
            isMoveFar = true;
        }

        if (bestMoveY < 2 && bestMoveY > -2)
        {
            yAxis = bestMoveY;
        }
        else
        {
            yAxis = bestMoveY / 2;
            isMoveFar = true;
        }

        if (xAxis == 0 && yAxis == 0)
        {
            xAxis = 1;
        }
    }
}
