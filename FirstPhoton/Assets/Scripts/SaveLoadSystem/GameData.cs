using System.Collections.Generic;
using UnityEngine;
using System;

namespace GDC.Managers
{
    [Serializable]
    public struct GameData
    {
        public bool IsSaveLoadProcessing;

        public bool IsHaveSaveData;
        public string PlayerName;
        public List<int> arr;
        public int p1i, p1j, p2i, p2j;

        public void SetupData() //load
        {
            IsSaveLoadProcessing = true;
            GameDataOrigin gameDataOrigin = SaveLoadManager.Instance.GameDataOrigin;

            IsHaveSaveData = gameDataOrigin.IsHaveSaveData;

            PlayerName = gameDataOrigin.PlayerName;
            if (arr == null)
            {
                arr = new List<int>();
            }
            arr = gameDataOrigin.arr;
            p1i = gameDataOrigin.p1i;
            p1j = gameDataOrigin.p1j;
            p2i = gameDataOrigin.p2i;
            p2j = gameDataOrigin.p2j;

            SaveLoadManager.Instance.GameDataOrigin = gameDataOrigin;
            IsSaveLoadProcessing = false;
        }
        public GameDataOrigin ConvertToGameDataOrigin() //save
        {
            IsSaveLoadProcessing = true;
            GameDataOrigin gameDataOrigin = new GameDataOrigin();

            gameDataOrigin.PlayerName = PlayerName;
            gameDataOrigin.arr = arr;
            gameDataOrigin.p1i = p1i;
            gameDataOrigin.p1j = p1j;
            gameDataOrigin.p2i = p2i;
            gameDataOrigin.p2j = p2j;

            IsSaveLoadProcessing = false;
            return gameDataOrigin;
        }

        #region suppert function
        #endregion
    }

    [Serializable]
    public struct GameDataOrigin
    {
        public bool IsHaveSaveData;
        public string PlayerName;

        public List<int> arr;
        public int p1i, p1j, p2i, p2j;
    }

    [Serializable]
    public struct CacheData
    {
        public bool isContinue;
    }
}
