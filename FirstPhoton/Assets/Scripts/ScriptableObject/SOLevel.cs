using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable Object/SO Level", fileName = "SO Level")]
public class SOLevel : ScriptableObject
{
    public List<string> gridStr;
}
