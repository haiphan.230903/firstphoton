using DG.Tweening;
using GDC.Managers;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;
using UnityEngine.Rendering.UI;

public class GridController : MonoBehaviour
{
    [Serializable]
    public class DataGrid
    {
        [ReadOnly] public int[] arr = new int[GameConstant.MAX_WIDTH]; //0: Trong, 1: Dung duoc, 2: Co vat can
        public SpriteRenderer[] transData = new SpriteRenderer[GameConstant.MAX_WIDTH];
    }

    public static GridController Instance { get; private set; }
    [SerializeField] SOLevel soLevel;
    public DataGrid[] dataGrid = new DataGrid[GameConstant.MAX_HEIGHT];
    //[SerializeField] Transform normalPlatform;
    [SerializeField, ReadOnly] Player player;
    public Player playerPrefab;
    public int playerListLength;
    [SerializeField] Color gridDefaultColor, player1Color, player2Color;

    [SerializeField] bool isOffline = false;

    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        LoadLevel();
    }

    private void Update()
    {
        playerListLength = PhotonNetwork.playerList.Length;
    }
    [PunRPC]
    void SyncGrid(string[] newDataGridString)
    {
        //dataGrid = newDataGridString;
        Debug.Log("Sync");
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                dataGrid[i].arr[j] = Convert.ToInt32(newDataGridString[i][j]) - 48;
                if (dataGrid[i].arr[j] == 0)
                {
                    dataGrid[i].transData[j].transform.DOScale(0, 0.5f);
                }
            }
        }
    }

    void CallSyncGrid()
    {
        PhotonView PV = GetComponent<PhotonView>();
        string[] newDataGridString = new string[GameConstant.MAX_HEIGHT];
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            newDataGridString[i] = "";
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                newDataGridString[i] += dataGrid[i].arr[j].ToString();
            }
        }
        if (PV != null)
            PV.RPC("SyncGrid", PhotonTargets.All, /*dataGrid*/newDataGridString);
    }
    public void SpawnPlayer(int turnId = 0, bool isBot = false, int playerPosi = -1, int playerPosj = -1)
    {
        if (isOffline == false)
        {
            player = PhotonNetwork.Instantiate(playerPrefab.name, transform.position, Quaternion.identity, 0).GetComponent<Player>();
        }
        else
        {
            player = Instantiate(playerPrefab, transform.position, Quaternion.identity).GetComponent<Player>();
        }

        int playerIndexi = 0, playerIndexj = 0;
        if (playerPosi == -1 && playerPosj == -1)
        {
            do
            {
                playerIndexi = UnityEngine.Random.Range(0, GameConstant.MAX_HEIGHT);
                playerIndexj = UnityEngine.Random.Range(0, GameConstant.MAX_WIDTH);
            }
            while (dataGrid[playerIndexi].arr[playerIndexj] != 1);
            dataGrid[playerIndexi].arr[playerIndexj] = 2;
        }
        else
        {
            playerIndexi = playerPosi;
            playerIndexj = playerPosj;
        }

        Color playerColor;
        //Debug.Log((isOffline == false && PhotonNetwork.playerList.Length == 1) || (isOffline && isBot == false));
        if ((isOffline == false && PhotonNetwork.playerList.Length == 1) || (isOffline && isBot == false))
        {
            playerColor = player1Color;
            if (isOffline == false)
            {
                GameplayController.instance.SetPlayer1Name(PhotonNetwork.playerName);
            }
            else
            {
                GameplayController.instance.SetPlayer1Name("Player");
            }
        }
        else
        {
            playerColor = player2Color;
            if (isOffline == false)
            {
                GameplayController.instance.SetPlayer2Name(PhotonNetwork.playerName);
            }
            else
            {
                GameplayController.instance.SetPlayer2Name("CPU");
            }
		}
		player.SetUp(playerIndexi, playerIndexj, dataGrid[playerIndexi].transData[playerIndexj].transform.position, playerColor);
        if (isOffline == false)
        {
            player.turnID = PhotonNetwork.playerList.Length;
            Debug.Log(player.turnID);
            GameplayController.instance.AddTurnID(player.turnID);
        }
        else
        {
            player.turnID = turnId;
            player.isBot = isBot;
            GameplayController.instance.AddTurnID(player.turnID);
        }

        if (isBot == false)
            dataGrid[playerIndexi].arr[playerIndexj] = 2;
        else
            dataGrid[playerIndexi].arr[playerIndexj] = 3;
        CallSyncGrid();
    }

    [Button]
    public void LoadLevel()
    {
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                dataGrid[i].arr[j] = Convert.ToInt32(soLevel.gridStr[i][j]) - 48;
                if (dataGrid[i].arr[j] == 0)
                {
                    dataGrid[i].transData[j].gameObject.SetActive(false);
                }
            }
        }

        if (isOffline == false)
        {
            SpawnPlayer();
        }
        else
        {
            SpawnPlayer(1,false);
            SpawnPlayer(2, true);
            if (SaveLoadManager.Instance.CacheData.isContinue)
            {
                LoadLevelFromData();
            }
        }
    }

    [Button]
    public void LoadObjData()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform childTransform = transform.GetChild(i);
            for (int j = 0; j < childTransform.childCount; j++)
            {
                dataGrid[i].transData[j] = childTransform.GetChild(j).GetComponent<SpriteRenderer>();
            }
        }
    }
    public void PassThrough(int i, int j, float delayDuration = 0, bool isMustDisappear = false)
    {
        if (isMustDisappear == false)
        {
            if (i < 0 || j < 0 || i >= GameConstant.MAX_HEIGHT || j >= GameConstant.MAX_WIDTH || dataGrid[i].arr[j] > 1) return;
        }
        dataGrid[i].arr[j] = 0;
        dataGrid[i].transData[j].transform.DOScale(0, 0.5f).SetDelay(delayDuration);
    }
    public void SetPlayerOnGrid(int i, int j, bool isBot = false)
    {
        if (i < 0 || j < 0 || i >= GameConstant.MAX_HEIGHT || j >= GameConstant.MAX_WIDTH) return;
        if (dataGrid[i].arr[j] == 0) return;
        
        if (isBot == false)
            dataGrid[i].arr[j] = 2;
        else
            dataGrid[i].arr[j] = 3;
        CallSyncGrid();
	}
    public void CheckPosCanMove(int i, int j, bool isMoveFar)
    {
        EndCheckPosCanMove(i, j);
        if (isMoveFar == false)
        {
            CheckPosAnim(i + 1, j);
            CheckPosAnim(i - 1, j);
            CheckPosAnim(i, j + 1);
            CheckPosAnim(i, j - 1);
        }
        else
        {
            CheckPosAnim(i + 2, j);
            CheckPosAnim(i - 2, j);
            CheckPosAnim(i, j + 2);
            CheckPosAnim(i, j - 2);
        }
    }
    public void EndCheckPosCanMove(int i, int j)
    {
        CheckPosAnim(i + 1, j, true);
        CheckPosAnim(i - 1, j, true);
        CheckPosAnim(i, j + 1, true);
        CheckPosAnim(i, j - 1, true);
        CheckPosAnim(i + 2, j, true);
        CheckPosAnim(i - 2, j, true);
        CheckPosAnim(i, j + 2, true);
        CheckPosAnim(i, j - 2, true);
    }
    void CheckPosAnim(int i, int j, bool isEndCheckPos = false)
    {
        if (IsPosInvalid(i,j))
            return;

        if (isEndCheckPos == false)
        {
            dataGrid[i].transData[j].DOColor((Color.blue + gridDefaultColor)/2, 0.75f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            DOTween.Kill(dataGrid[i].transData[j]);
            dataGrid[i].transData[j].color = gridDefaultColor;
        }
    }
    public bool IsPosInvalid(int i, int j)
    {
        return i < 0 || j < 0 || i >= GameConstant.MAX_HEIGHT || j >= GameConstant.MAX_WIDTH || dataGrid[i].arr[j] != 1;
    }
    public int NumberMoveValid(int i, int j)
    {
        int num = 0;
        if (IsPosInvalid(i + 1, j) == false) num++;
        if (IsPosInvalid(i - 1, j) == false) num++;
        if (IsPosInvalid(i, j + 1) == false) num++;
        if (IsPosInvalid(i, j - 1) == false) num++;
        if (IsPosInvalid(i + 2, j) == false) num++;
        if (IsPosInvalid(i - 2, j) == false) num++;
        if (IsPosInvalid(i, j + 2) == false) num++;
        if (IsPosInvalid(i, j - 2) == false) num++;
        return num;
    }
    public DataGrid[] SimulateGrid(DataGrid[] curGrid, int xMove, int yMove, int pi, int pj, bool isBot) //Nuoc di da hop le
    {
        DataGrid[] newGrid = new DataGrid[GameConstant.MAX_HEIGHT];
        for (int i = 0; i< GameConstant.MAX_HEIGHT; i++)
        {
            newGrid[i] = new DataGrid();
            newGrid[i].arr = new int[GameConstant.MAX_WIDTH];
            for (int j =0; j<GameConstant.MAX_WIDTH; j++)
            {
                newGrid[i].arr[j] = curGrid[i].arr[j];
            }
        }
        newGrid[pi].arr[pj] = 0;
        //if ((pi - yMove >= 0) && (pj + xMove >= 0) && (pi - yMove < GameConstant.MAX_HEIGHT) && (pj + xMove < GameConstant.MAX_WIDTH))
        {
            Debug.Log(xMove + " " + yMove + " " + (pi - yMove) + " " + (pj + xMove));
            newGrid[pi - yMove / 2].arr[pj + xMove / 2] = 0;

            if (isBot)
            {
                newGrid[pi - yMove].arr[pj + xMove] = 3;
            }
            else
            {
                newGrid[pi - yMove].arr[pj + xMove] = 2;
            }
        }
        return newGrid;
    }

    [Button]
    void SaveGridToData()
    {
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                if (SaveLoadManager.Instance.GameData.arr == null)
                {
                    SaveLoadManager.Instance.GameData.arr = new List<int>();
                }
                SaveLoadManager.Instance.GameData.arr.Add(dataGrid[i].arr[j]);
                //dataGrid[i].arr[j] = Convert.ToInt32(soLevel.gridStr[i][j]) - 48;
                //if (dataGrid[i].arr[j] == 0)
                //{
                //    dataGrid[i].transData[j].gameObject.SetActive(false);
                //}
            }
        }
        Player[] players = FindObjectsOfType<Player>();
        foreach (var player in players)
        {
            if (player.isBot == false)
            {
                SaveLoadManager.Instance.GameData.p1i = player.playerIndexi;
                SaveLoadManager.Instance.GameData.p1j = player.playerIndexj;
            }
            else
            {
                SaveLoadManager.Instance.GameData.p2i = player.playerIndexi;
                SaveLoadManager.Instance.GameData.p2j = player.playerIndexj;
            }
        }    
    }

    [Button]
    public void LoadLevelFromData()
    {
        GameData gameData = SaveLoadManager.Instance.GameData;
        for (int i = 0; i < GameConstant.MAX_HEIGHT; i++)
        {
            for (int j = 0; j < GameConstant.MAX_WIDTH; j++)
            {
                dataGrid[i].arr[j] = gameData.arr[i*GameConstant.MAX_WIDTH+j];
                if (dataGrid[i].arr[j] == 0)
                {
                    dataGrid[i].transData[j].gameObject.SetActive(false);
                }
                else
                {
                    dataGrid[i].transData[j].gameObject.SetActive(true);
                    dataGrid[i].transData[j].transform.localScale = Vector3.one;
                    DOTween.Kill(dataGrid[i].transData[j]);
                }
            }
        }

        //if (isOffline == false)
        //{
        //    SpawnPlayer();
        //}
        //else
        {
            Player[] players = FindObjectsOfType<Player>();
            foreach (var player in players)
            {
                if (player.isBot == false)
                {
                    player.playerIndexi = gameData.p1i;
                    player.playerIndexj = gameData.p1j;
                    player.transform.position = dataGrid[gameData.p1i].transData[gameData.p1j].transform.position;
                }
                else
                {
                    player.playerIndexi = gameData.p2i;
                    player.playerIndexj = gameData.p2j;
                    player.transform.position = dataGrid[gameData.p2i].transData[gameData.p2j].transform.position;
                }
            }
        }
    }
}
