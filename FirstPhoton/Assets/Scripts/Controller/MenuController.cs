using GDC.Managers;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] string versionName = "0.1";

    [SerializeField] TMP_InputField usernameInput, createRoomInput, joinRoomInput;
    [SerializeField] GameObject waitingPanel;

    private void Awake()
    {
        PhotonNetwork.ConnectUsingSettings(versionName);
    }

    private void Start()
    {
        StartCoroutine(Cor_Load());
    }
    IEnumerator Cor_Load()
    {
        yield return new WaitUntil(() => SaveLoadManager.Instance != null);
        SaveLoadManager.Instance.Load();
    }
    private void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        Debug.Log("Connected");
    }

    void SetUsername()
    {
        PhotonNetwork.playerName = usernameInput.text;
    }

    public void CreateRoom()
    {
        waitingPanel.SetActive(true);
        SetUsername();
        PhotonNetwork.CreateRoom(createRoomInput.text, new RoomOptions() { maxPlayers = 2 }, null);
    }    
    public void JoinGame()
    {
		waitingPanel.SetActive(true);
		SetUsername();
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.maxPlayers = 2;
        PhotonNetwork.JoinOrCreateRoom(joinRoomInput.text, roomOptions, TypedLobby.Default);
    }
    void OnJoinedRoom()
    {
        PhotonNetwork.LoadLevel("Gameplay");
    }

    public void NewOfflineGameplay()
    {
        SceneManager.LoadScene(2);
        SaveLoadManager.Instance.CacheData.isContinue = false;
    }
    public void ContinueOfflineGameplay()
    {
        SceneManager.LoadScene(2);
        SaveLoadManager.Instance.CacheData.isContinue = true;
    }
}
