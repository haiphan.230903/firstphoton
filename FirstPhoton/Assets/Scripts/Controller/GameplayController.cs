using DG.Tweening;
using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameplayController : MonoBehaviour
{
	public static GameplayController instance { get; private set; }
	[SerializeField] PhotonView photonView;
	public int turnIDMax;
	public int currentTurn;
	[SerializeField] int numberOfPlayerRequire;
	[SerializeField] GameObject waitingUI, gameplayUI, waitingPanel;
	[SerializeField] TMP_Text turnText, player1Name, player2Name;
	public Enums.GameStatus gameStatus;
	[ReadOnly] public int thisPlayerID;

	[SerializeField] bool isOffline = false;

	private void Awake()
	{
		instance = this;
	}
	
	public void AddTurnID(int id)
	{
		thisPlayerID = id;
		PhotonView PV = GetComponent<PhotonView>();
		if (PV != null)
		{
			PV.RPC("SyncTurnIDMax", PhotonTargets.All, id);
		}
		else
		{
			SyncTurnIDMax(id);
		}
	}
	[PunRPC]
	void StartGame()
	{
		currentTurn = UnityEngine.Random.Range(0, 100 % turnIDMax) + 1;
		PhotonView PV = GetComponent<PhotonView>();
		if (PV != null)
		{
			PV.RPC("SyncTurn", PhotonTargets.All, currentTurn);
		}
		else
		{
			currentTurn = 1;
			SyncTurn(currentTurn);
		}
	}

	[PunRPC]
	void SyncTurn(int turn)
	{
		//dataGrid = newDataGridString;
		Debug.Log("Sync turn");
		currentTurn = turn;
		if (currentTurn == 1)
		{
			turnText.text = "Blue turn!";
		}
		else
		{
			turnText.text = "Red turn!";
		}
		turnText.transform.DOScale(1.2f, 0.5f).SetLoops(4, LoopType.Yoyo);
	}
	[PunRPC]
	void SyncTurnIDMax(int id)
	{
		//dataGrid = newDataGridString;
		Debug.Log("Sync turnIdMax");
		turnIDMax = id;
		if (turnIDMax < numberOfPlayerRequire)
		{
			gameStatus = Enums.GameStatus.Waiting;
			waitingUI.SetActive(true);
			gameplayUI.SetActive(false);
		}
		else
		{
			gameStatus = Enums.GameStatus.Playing;
			waitingUI.SetActive(false);
			gameplayUI.SetActive(true);
			StartGame();
		}
	}

	[PunRPC]
	void SyncEndGame(int playerLoseId)
	{
		Debug.Log("Sync EndGame");
		gameStatus = Enums.GameStatus.End;
		if (isOffline == false)
		{
			if (thisPlayerID == playerLoseId)
			{
				turnText.text = "YOU LOSE";
			}
			else
			{
				turnText.text = "YOU WIN";
			}
		}
		else
		{
			if (playerLoseId == 1)
            {
                turnText.text = "YOU LOSE";
            }
            else
            {
                turnText.text = "YOU WIN";
            }
        }
		turnText.transform.DOScale(1.5f, 0.5f).SetLoops(-1, LoopType.Yoyo);
	}
	[PunRPC]
	void SyncPlayer1Name(string name)
	{
		player1Name.text = name;
	}
	[PunRPC]
	void SyncPlayer2Name(string name)
	{
		player2Name.text = name;
	}
	public void SetPlayer1Name(string name)
	{
		PhotonView PV = GetComponent<PhotonView>();
		if (PV != null)
		{
			PV.RPC("SyncPlayer1Name", PhotonTargets.All, name);
		}
		else
		{
            player1Name.text = name;
        }
	}
	public void SetPlayer2Name(string name)
	{
		PhotonView PV = GetComponent<PhotonView>();
		if (PV != null)
		{
			PV.RPC("SyncPlayer2Name", PhotonTargets.All, name);
		}
		else
		{
            player2Name.text = name;
        }
	}
	public void NextTurn()
	{
		PhotonView PV = GetComponent<PhotonView>();
		currentTurn++;
		if (currentTurn>turnIDMax)
		{
			currentTurn = 1;
		}
		if (PV != null)
		{
			PV.RPC("SyncTurn", PhotonTargets.All, currentTurn);
		}
	}

	public void EndGame(int playerLoseId)
	{
		PhotonView PV = GetComponent<PhotonView>();
		if (PV != null)
		{
			PV.RPC("SyncEndGame", PhotonTargets.All, playerLoseId);
		}
		else
		{
			SyncEndGame(playerLoseId);
		}
	}
	public void LeaveRoom()
	{
		waitingPanel.SetActive(true);
		PhotonNetwork.LeaveRoom();
		PhotonNetwork.LoadLevel(0);
	}
}
